import 'package:flutter/material.dart';
import 'dart:math' as math;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final radius = math.min(size.width, size.height) / 2;
    final center = Offset(size.width / 2, size.height / 2);
    final color = Paint()..color = Colors.yellow;

    canvas.drawCircle(center, radius, color);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    throw null;
  }
}

class _MyHomePageState extends State<MyHomePage> {
  final customPaint = CustomPaint(
    painter: Painter(),
  );

  bool isReversed = false;

  List<Widget> createList() {
    return [
      ...List<Widget>.generate(
        100,
        (int index) {
          return Container(
            height: 50,
            color: Colors.blue,
            margin: EdgeInsets.all(3),
            child: Center(
              child: Text(
                index.toString(),
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
          );
        },
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: SizedBox(
              height: MediaQuery.of(context).size.width * 0.5,
              width: MediaQuery.of(context).size.width * 0.5,
              child: customPaint,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      isReversed = !isReversed;
                    });
                  },
                  child: Text('Change sort'),
                ),
                Container(
                  height: 200,
                  width: 75,
                  child: (ListView(
                    children: isReversed ? createList().reversed.toList() : createList()
                  )),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
